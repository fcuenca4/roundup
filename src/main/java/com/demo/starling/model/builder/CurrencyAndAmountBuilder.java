package com.demo.starling.model.builder;

import com.demo.starling.model.CurrencyAndAmount;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CurrencyAndAmountBuilder {
    private ObjectFactory<CurrencyAndAmount> currencyAndAmountObjectFactory;

    public CurrencyAndAmount build(String currency, BigDecimal minorUnits) {
        CurrencyAndAmount currencyAndAmount = currencyAndAmountObjectFactory.getObject();
        currencyAndAmount.setCurrency(currency);
        currencyAndAmount.setMinorUnits(minorUnits);
        return currencyAndAmount;
    }

    @Autowired
    public void setCurrencyAndAmountObjectFactory(ObjectFactory<CurrencyAndAmount> currencyAndAmountObjectFactory) {
        this.currencyAndAmountObjectFactory = currencyAndAmountObjectFactory;
    }
}