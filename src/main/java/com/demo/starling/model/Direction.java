package com.demo.starling.model;

public enum Direction {
    OUTBOUND,
    INBOUND,
    NONE
}
