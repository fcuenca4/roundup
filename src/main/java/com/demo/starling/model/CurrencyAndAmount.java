package com.demo.starling.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;


@Scope("prototype")
@Component
public class CurrencyAndAmount {

    @NotBlank(message = "The value must not be null or blank")
    private String currency;
    @Positive(message = "The value must be positive")
    @Digits(integer = 100, fraction = 0)
    private BigDecimal minorUnits; //int64


    public CurrencyAndAmount() {
    }

    public CurrencyAndAmount(String currency, BigDecimal minorUnits) {
        this.currency = currency;
        this.minorUnits = minorUnits;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getMinorUnits() {
        return minorUnits;
    }

    public void setMinorUnits(BigDecimal minorUnits) {
        this.minorUnits = minorUnits;
    }
}


