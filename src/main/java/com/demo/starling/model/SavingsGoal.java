package com.demo.starling.model;

import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Component
public class SavingsGoal {
    @NotBlank
    private String name;
    @NotBlank
    private String currency;
    @NotNull
    @Valid
    private CurrencyAndAmount currencyAndAmount;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public CurrencyAndAmount getCurrencyAndAmount() {
        return currencyAndAmount;
    }

    public void setCurrencyAndAmount(CurrencyAndAmount currencyAndAmount) {
        this.currencyAndAmount = currencyAndAmount;
    }


}
