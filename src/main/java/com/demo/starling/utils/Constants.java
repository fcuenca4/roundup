package com.demo.starling.utils;

import java.time.format.DateTimeFormatter;

public final class Constants {
    public static final Integer TRANSACTION_QUERY_DATE_INTERVAL = 7;
    public static final DateTimeFormatter SIGNATURE_TIMESTAMP_FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    public static final String AUTHORIZATION = "Authorization";
    public static final String DATE = "Date";
    public static final String DIGEST = "Digest";



}


