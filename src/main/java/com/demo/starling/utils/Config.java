package com.demo.starling.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/*
 * Config file to map application.properties in execution time.
 * This are used allover the project.
 * There's lot of improvements here, for example:
 * find a way making this class static (Spring can't inject on static context) or using custom YAML file to load configurations.
 * */
@Service
public class Config {
    @Value("${config.privateKeyPath}")
    public String privateKeyPath;
    @Value("${config.apiKeyUid}")
    private String apiKeyUid;
    @Value("${config.accessToken}")
    private String accessToken;
    @Value("${resource.hostname}")
    private String hostname;
    @Value("${resource.savings}")
    private String savingsResourceUrlPath;
    @Value("${resource.transactions}")
    private String transactionsResourceUrlPath;
    @Value("${resource.account}")
    private String accountResourceUrlPath;
    @Value("${config.accountUid}")
    private String accountUid;
    @Value("${resource.add_money}")
    private String addMoneyResourceUrlPath;

    public String getApiKeyUid() {
        return apiKeyUid;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getHostname() {
        return hostname;
    }

    public String getSavingsResourceUrlPath() {
        return savingsResourceUrlPath;
    }

    public String getTransactionsResourceUrlPath() {
        return transactionsResourceUrlPath;
    }

    public String getPrivateKeyPath() {
        return privateKeyPath;
    }

    public String getAccountResourceUrlPath() {
        return accountResourceUrlPath;
    }

    public String getAccountUid() {
        return accountUid;
    }

    public String getAddMoneyResourceUrlPath() {
        return addMoneyResourceUrlPath;
    }
}
