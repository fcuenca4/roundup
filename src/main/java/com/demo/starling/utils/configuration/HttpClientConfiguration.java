package com.demo.starling.utils.configuration;

import org.apache.http.client.HttpClient;
import org.apache.http.message.BasicHeader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class HttpClientConfiguration {

    @Bean
     HttpClient httpClient() {
        return org.apache.http.impl.client.HttpClientBuilder.create()
                .setDefaultHeaders(Arrays.asList(
                        new BasicHeader("Content-Type", "application/json"),
                        new BasicHeader("Accept", "application/json")))
                .build();
    }
}
