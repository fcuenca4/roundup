package com.demo.starling.service;

import com.demo.starling.utils.Config;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.security.*;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

/*
 * CredentialService is responsible to handle credential encryption/decryption ,
 * also calculates digest and loads  private key
 * */
@Service
public class CredentialsService {

    private Config config;

    public String getAccessTokenAuthorizationHeader() {
        return "Bearer " + config.getAccessToken();
    }

    public String calculateDigest(String payload) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
        byte[] payloadBytes = payload.getBytes();
        return Base64.getEncoder().encodeToString(messageDigest.digest(payloadBytes));
    }

    public String calculateAuthorizationHeader(String textToSign) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        PrivateKey privateKey = loadPrivateKey();
        Signature instance = Signature.getInstance("SHA512withRSA"); // Could also use "SHA512withECDSA"
        instance.initSign(privateKey);
        instance.update(textToSign.getBytes());
        byte[] encodedSignedString = Base64.getEncoder().encode(instance.sign());
        String signature = new String(encodedSignedString);
        return "Bearer " + config.getAccessToken() + ";Signature keyid=\"" + config.getApiKeyUid() + "\",algorithm=\"rsa-sha512\",headers=\"(request-target) Date Digest\",signature=\"" + signature + "\"";
    }


    public PrivateKey loadPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] privateKeyBytes = FileUtils.readFileToByteArray(new File(config.getPrivateKeyPath()));
        EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(privateKeySpec);
    }

    @Autowired
    public void setConfig(Config config) {
        this.config = config;
    }
}
