package com.demo.starling.service;

import com.demo.starling.model.Direction;
import com.demo.starling.model.Transaction;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/*
 * RoundUpService encapsulates business logic related to the problem.
 *
 * */
@Service
public class RoundUpService {
    /*
     * This method calculates savings doing a round-up to the nearest currency,
     * inside starling API, savings are mapped to OUTBOUND direction (from the customer account, to another).
     * I've worked with BigDecimal values because of scalability
     * @param transactions This is a list of transactions within an time specific offset
     * @return BigDecimal This value is the savings.
     * */
    public BigDecimal calculateSavings(List<Transaction> transactions) {
        return transactions.stream()
                .filter(o -> o.getDirection().equals(Direction.OUTBOUND))
                .map(o -> {
                    BigDecimal outBoundAmount = o.getAmount().abs();
                    BigDecimal rounded = outBoundAmount.setScale(0, RoundingMode.UP);
                    return rounded.subtract(outBoundAmount);
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);
    }
}
