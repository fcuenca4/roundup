package com.demo.starling.service;

import com.demo.starling.model.Transaction;
import com.demo.starling.service.request.StarlingBankRequestService;
import com.demo.starling.utils.Config;
import com.demo.starling.utils.Constants;
import exception.ExceptionFactory;
import exception.ExceptionType;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

/*
 * TransactionService is responsible for getting N transactions during an specific date (7 days for this case)
 * Internally, is managed using a list.
 * */
@Service
public class TransactionService {

    private StarlingBankRequestService starlingBankRequestHandler;
    private Config config;

    /*
     * getTransactions queries Starling Bank API
     * */
    public List<Transaction> getTransactions() {
        Date[] interval = generateTransactionInterval();
        Date fromDate = interval[0];
        Date toDate = interval[1];
        return starlingBankRequestHandler.getTransactions(buildUrl(fromDate, toDate));
    }

    private static Date[] generateTransactionInterval() {
        Date toDate = new Date();
        LocalDateTime localDateTime = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.minusDays(Constants.TRANSACTION_QUERY_DATE_INTERVAL);
        Date fromDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return new Date[]{fromDate, toDate};
    }

    /*
     * @param from Date
     * @param to Date
     * return url String
     * */
    private String buildUrl(Date from, Date to) {
        URIBuilder builder;
        try {
            builder = new URIBuilder(config.getHostname() + config.getTransactionsResourceUrlPath());
        } catch (URISyntaxException ex) {
            throw ExceptionFactory.create(ExceptionType.HTTP_BAD_REQUEST);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        builder.setParameter("from", sdf.format(from));
        builder.setParameter("to", sdf.format(to));
        try {
            return builder.build().toString();
        } catch (URISyntaxException ex) {
            throw ExceptionFactory.create(ExceptionType.HTTP_BAD_REQUEST);
        }
    }

    @Autowired
    public void setConfig(Config config) {
        this.config = config;
    }

    @Autowired
    public void setStarlingBankRequestHandler(StarlingBankRequestService starlingBankRequestHandler) {
        this.starlingBankRequestHandler = starlingBankRequestHandler;
    }
}
