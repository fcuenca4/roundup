package com.demo.starling.service.request;

import com.demo.starling.model.CurrencyAndAmount;
import com.demo.starling.model.Transaction;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import exception.ExceptionFactory;
import exception.ExceptionType;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/*
 * StarlingBankRequestHandler execute starling's business api call to complete interview requirements.
 * This class only manages to handle few logic. If problem is bigger, i must recommend to divide this class
 * using Single Responsibility Principle i.e divide and implement classed based on specific logic.
 * */
@Component
public class StarlingBankRequestService {
    private RequestService requestHandler;

    private static List<Transaction> parseResponse(HttpResponse response) {
        String jsonString;
        try {
            jsonString = EntityUtils.toString(response.getEntity());
        } catch (IOException ex) {
            throw ExceptionFactory.create(ExceptionType.HTTP_INTERNAL_SERVER_ERROR);
        }
        JsonObject jsonMsg = new JsonParser().parse(jsonString).getAsJsonObject();
        JsonObject jsonEmbedded = jsonMsg.getAsJsonObject("_embedded");
        Transaction[] transactionArray = new Gson().fromJson(jsonEmbedded.getAsJsonArray("transactions"), Transaction[].class);
        return Arrays.asList(transactionArray);
    }

    public UUID addMoney(CurrencyAndAmount currencyAndAmount, String resourcePath) {
        String payload = createPayload(currencyAndAmount);
        HttpResponse response = requestHandler.put(resourcePath, payload);
        return parseResponse(response, "transferUid");
    }

    private String createPayload(CurrencyAndAmount currencyAndAmount) {
        JsonElement payloadJsonTree = new Gson().toJsonTree(currencyAndAmount);
        JsonObject payloadJsonObject = new JsonObject();
        payloadJsonObject.add("amount", payloadJsonTree);
        return payloadJsonObject.toString();
    }

    public UUID createSavingsGoal(String payload, String resourcePath) {
        HttpResponse response = requestHandler.put(resourcePath, payload);
        return parseResponse(response, "savingsGoalUid");
    }

    public List<Transaction> getTransactions(String resourcePath) {
        HttpResponse response = requestHandler.get(resourcePath);
        return parseResponse(response);

    }

    private UUID parseResponse(HttpResponse response, String filter) {
        String jsonString;
        try {
            jsonString = EntityUtils.toString(response.getEntity());
        } catch (IOException ex) {
            throw ExceptionFactory.create(ExceptionType.HTTP_INTERNAL_SERVER_ERROR);
        }
        JsonObject jsonMsg = new JsonParser().parse(jsonString).getAsJsonObject();
        return new Gson().fromJson(jsonMsg.get(filter), UUID.class);
    }


    @Autowired
    public void setRequestHandler(RequestService requestHandler) {
        this.requestHandler = requestHandler;
    }
}
