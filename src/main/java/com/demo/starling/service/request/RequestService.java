package com.demo.starling.service.request;

import com.demo.starling.service.CredentialsService;
import com.demo.starling.utils.Constants;
import exception.ExceptionFactory;
import exception.ExceptionType;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
/*
 * RequestHandler responsibility is to handle
 * HttpRequests and credentials generation, Digest and Authorization calculation
 * */
public final class RequestService {

    private CredentialsService credentialsService;

    @Autowired
    private HttpClient httpClient;

    public HttpResponse put(String resourcePath, String payloadJson) {
        String timestamp = Constants.SIGNATURE_TIMESTAMP_FORMATTER.format(ZonedDateTime.now());
        HttpPut put;
        HttpResponse response;
        try {
            String digest = credentialsService.calculateDigest(payloadJson);
            String textToSign = "(request-target): " + HttpMethod.PUT + " " + resourcePath + "\nDate: " + timestamp + "\nDigest: " + digest;
            String authorizationHeader = credentialsService.calculateAuthorizationHeader(textToSign);
            put = new HttpPut(new URIBuilder(resourcePath)
                    .build());
            put.setEntity(new StringEntity(payloadJson));
            put.addHeader(Constants.AUTHORIZATION, authorizationHeader);
            put.addHeader(Constants.DATE, timestamp);
            put.addHeader(Constants.DIGEST, digest);
            response = httpClient.execute(put);
        } catch (Exception ex) {
            throw ExceptionFactory.create(ExceptionType.HTTP_INTERNAL_SERVER_ERROR);
        }
        validateResponse(response);
        return response;
    }

    public HttpResponse get(String resourcePath) {
        HttpResponse response;
        try {
            HttpGet get = new HttpGet(new URIBuilder(resourcePath)
                    .build());
            String timestamp = Constants.SIGNATURE_TIMESTAMP_FORMATTER.format(ZonedDateTime.now());
            get.addHeader(Constants.AUTHORIZATION, credentialsService.getAccessTokenAuthorizationHeader());
            get.addHeader(Constants.DATE, timestamp);
            get.addHeader(Constants.DIGEST, "");
            response = httpClient.execute(get);
        } catch (Exception ex) {
            throw ExceptionFactory.create(ExceptionType.HTTP_INTERNAL_SERVER_ERROR);
        }
        validateResponse(response);
        return response;
    }


    private void validateResponse(HttpResponse response) {
        int statusCode = response.getStatusLine().getStatusCode();
        switch (statusCode) {
            case HttpStatus.SC_OK:
                break;
            case HttpStatus.SC_CREATED:
                break;
            case HttpStatus.SC_NOT_FOUND:
                throw ExceptionFactory.create(ExceptionType.HTTP_BAD_REQUEST);
            case HttpStatus.SC_FORBIDDEN:
                throw ExceptionFactory.create(ExceptionType.HTTP_FORBIDDEN);
            default:
                throw ExceptionFactory.create(ExceptionType.HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    public void setCredentialsService(CredentialsService credentialsService) {
        this.credentialsService = credentialsService;
    }
}
