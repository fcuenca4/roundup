package com.demo.starling.service;

import com.demo.starling.model.CurrencyAndAmount;
import com.demo.starling.model.SavingsGoal;
import com.demo.starling.model.builder.CurrencyAndAmountBuilder;
import com.demo.starling.service.request.StarlingBankRequestService;
import com.demo.starling.utils.Config;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

/*
 *
 * SavingsGoalService is responsible to maintain all actions needed solve the problem
 *
 * */
@Service
public class SavingsGoalService {

    private Config config;
    private CurrencyAndAmountBuilder currencyAndAmountBuilder;
    private StarlingBankRequestService starlingBankRequestHandler;

    /*
     * creates a new SavingsGoal.
     * @param savingsGoal SavingsGoal
     * @return UUid  id of the new SavingsGoal.
     * */
    public UUID create(SavingsGoal savingsGoal) {
        String resourcePath = config.getHostname() +
                config.getAccountResourceUrlPath() +
                config.getAccountUid() +
                "/" +
                config.getSavingsResourceUrlPath();
        String payloadJson = new Gson().toJson(savingsGoal);
        return starlingBankRequestHandler.createSavingsGoal(payloadJson, resourcePath);
    }

    /*
     * adds money to a specific savings Goal
     * @param quantity BigDecimal
     * @param currency String
     * @param savingsGoalUID UUID
     *
     * @return UUid id of the new transfer.
     * */
    public UUID addMoney(BigDecimal quantity, String currency, UUID savingsGoalUID) {
        String resourcePath = config.getHostname() +
                config.getAccountResourceUrlPath() +
                config.getAccountUid() +
                "/" +
                config.getSavingsResourceUrlPath() +
                savingsGoalUID +
                "/" +
                config.getAddMoneyResourceUrlPath() +
                UUID.randomUUID();
        BigDecimal quantityUnits = new BigDecimal(quantity.toString().replace(".", "")); //Convert to minor units
        CurrencyAndAmount currencyAndAmount = currencyAndAmountBuilder.build(currency, quantityUnits);
        return starlingBankRequestHandler.addMoney(currencyAndAmount, resourcePath);
    }


    @Autowired
    public void setStarlingBankRequestHandler(StarlingBankRequestService starlingBankRequestHandler) {
        this.starlingBankRequestHandler = starlingBankRequestHandler;
    }

    @Autowired
    public void setConfig(Config config) {
        this.config = config;
    }

    @Autowired
    public void setCurrencyAndAmountBuilder(CurrencyAndAmountBuilder currencyAndAmountBuilder) {
        this.currencyAndAmountBuilder = currencyAndAmountBuilder;
    }
}
