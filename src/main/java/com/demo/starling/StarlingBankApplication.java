package com.demo.starling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarlingBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarlingBankApplication.class, args);
    }

}
