package com.demo.starling.controller;

import com.demo.starling.model.SavingsGoal;
import com.demo.starling.model.Transaction;
import com.demo.starling.service.RoundUpService;
import com.demo.starling.service.SavingsGoalService;
import com.demo.starling.service.TransactionService;
import com.demo.starling.utils.Constants;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class RoundUpController {
    private RoundUpService roundUpService;
    private TransactionService transactionService;
    private SavingsGoalService savingsGoalService;

    private static JsonObject buildResponse(UUID transferUid, UUID savingsGoalUID, BigDecimal roundUpValue) {
        JsonObject response = new JsonObject();
        response.addProperty("transferUid", transferUid.toString());
        response.addProperty("savingsGoalUid", savingsGoalUID.toString());
        response.addProperty("round-up", roundUpValue);
        return response;
    }


    @PutMapping("/starling/roundup")
    public ResponseEntity<?> triggerRoundUp(@RequestBody @Valid SavingsGoal savingsGoal) {
        List<Transaction> transactions = transactionService.getTransactions();
        BigDecimal roundUpValue = roundUpService.calculateSavings(transactions);
        UUID savingsGoalUID = savingsGoalService.create(savingsGoal);
        UUID transferUid = savingsGoalService.addMoney(roundUpValue, savingsGoal.getCurrency(), savingsGoalUID);
        JsonObject response = buildResponse(transferUid, savingsGoalUID, roundUpValue);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
     * Single responsibility principle
     * */
    @Autowired
    public void setRoundUpService(RoundUpService roundUpService) {
        this.roundUpService = roundUpService;
    }

    @Autowired
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Autowired
    public void setSavingsGoalService(SavingsGoalService savingsGoalService) {
        this.savingsGoalService = savingsGoalService;
    }
}
