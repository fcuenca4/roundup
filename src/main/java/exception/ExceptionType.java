package exception;

import org.springframework.http.HttpStatus;

public enum ExceptionType {

    HTTP_INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "An internal server error occurred."),
    HTTP_BAD_REQUEST(HttpStatus.BAD_REQUEST, "Not found."),
    HTTP_FORBIDDEN(HttpStatus.FORBIDDEN, "Invalid credentials.");


    //you can specify your own exception types...

    private HttpStatus status;
    private String message;

    ExceptionType(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}