package unit;

import com.demo.starling.service.CredentialsService;
import com.demo.starling.service.request.RequestService;
import exception.ApplicationSpecificException;
import exception.ExceptionType;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.doReturn;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.net.ssl.*", "javax.security.auth.x500.X500Principal"})

public class RequestServiceUnitTest {
    @InjectMocks
    RequestService requestHandler;
    @Mock
    CredentialsService credentialsService;
    @Mock
    HttpClient httpClient;
    @Mock
    HttpResponse httpResponse;
    @Mock
    StatusLine statusLine;

    private String payload;
    private String digest;
    private String resourcePath;
    private String authorizationHeader;

    @Before
    public void setup() {
        // initialize all the @Mock objects
        MockitoAnnotations.initMocks(this);
        payload = "{\n" +
                "    \"name\": \"fcuenca\",\n" +
                "    \"name\": \"GBP\",\n" +
                "\t\"currencyAndAmount\": {\n" +
                "    \"currency\":\"GBP\",\n" +
                "    \"currency\":\"30000\"\n" +
                "  }\n" +
                "}";
        digest = "I'm a Digest !";
        resourcePath = "https://api-sandbox.starlingbank.com/api/v2/account/70d00b0f-19e8-46fb-9042-9b55f48e2180/savings-goals/70d00b0f-19e8-46fb-9042-9b55f48e2246";
        authorizationHeader = "I'm Authorized and signed, believe me pls";
        try {
            doReturn(authorizationHeader).when(credentialsService).calculateAuthorizationHeader(Mockito.anyString());
            doReturn(digest).when(credentialsService).calculateDigest(payload);
            doReturn(httpResponse).when(httpClient).execute(ArgumentMatchers.any());
            doReturn(statusLine).when(httpResponse).getStatusLine();
        } catch (Exception ex) {
            ex.printStackTrace();
            assert false;
        }
    }

    @Test
    public void httpPut_Valid() {
        doReturn(HttpStatus.SC_CREATED).when(statusLine).getStatusCode();
        requestHandler.put(resourcePath, payload);
    }


    @Test
    public void httpPut_403() {
        try {
            doReturn(HttpStatus.SC_FORBIDDEN).when(statusLine).getStatusCode();
            requestHandler.put(resourcePath, payload);
        } catch (Exception ex) {
            assert ex instanceof ApplicationSpecificException;
            assert ((ApplicationSpecificException) ex).getExceptionType() == ExceptionType.HTTP_FORBIDDEN;
        }
    }

    @Test
    public void httpPut_404() {
        try {
            doReturn(HttpStatus.SC_NOT_FOUND).when(statusLine).getStatusCode();
            requestHandler.put(resourcePath, payload);
        } catch (Exception ex) {
            assert ex instanceof ApplicationSpecificException;
            assert ((ApplicationSpecificException) ex).getExceptionType() == ExceptionType.HTTP_BAD_REQUEST;
        }
    }

    @Test
    public void httpPut_else() {
        try {
            doReturn(HttpStatus.SC_INTERNAL_SERVER_ERROR).when(statusLine).getStatusCode();
            requestHandler.put(resourcePath, payload);
        } catch (Exception ex) {
            assert ex instanceof ApplicationSpecificException;
            assert ((ApplicationSpecificException) ex).getExceptionType() == ExceptionType.HTTP_INTERNAL_SERVER_ERROR;
        }
    }
    @Test
    public void httpGet_Valid() {
        doReturn(HttpStatus.SC_CREATED).when(statusLine).getStatusCode();
        requestHandler.get(resourcePath);
    }

    @Test
    public void httpGet_403() {
        try {
            doReturn(HttpStatus.SC_FORBIDDEN).when(statusLine).getStatusCode();
            requestHandler.get(resourcePath);
        } catch (Exception ex) {
            assert ex instanceof ApplicationSpecificException;
            assert ((ApplicationSpecificException) ex).getExceptionType() == ExceptionType.HTTP_FORBIDDEN;
        }
    }

    @Test
    public void httpGet_404() {
        try {
            doReturn(HttpStatus.SC_NOT_FOUND).when(statusLine).getStatusCode();
            requestHandler.get(resourcePath);
        } catch (Exception ex) {
            assert ex instanceof ApplicationSpecificException;
            assert ((ApplicationSpecificException) ex).getExceptionType() == ExceptionType.HTTP_BAD_REQUEST;
        }
    }

    @Test
    public void httpGet_else() {
        try {
            doReturn(HttpStatus.SC_INTERNAL_SERVER_ERROR).when(statusLine).getStatusCode();
            requestHandler.get(resourcePath);
        } catch (Exception ex) {
            assert ex instanceof ApplicationSpecificException;
            assert ((ApplicationSpecificException) ex).getExceptionType() == ExceptionType.HTTP_INTERNAL_SERVER_ERROR;
        }
    }

}
