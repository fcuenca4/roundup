package unit;

import com.demo.starling.model.CurrencyAndAmount;
import com.demo.starling.model.SavingsGoal;
import com.demo.starling.model.builder.CurrencyAndAmountBuilder;
import com.demo.starling.service.SavingsGoalService;
import com.demo.starling.service.request.StarlingBankRequestService;
import com.demo.starling.utils.Config;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.UUID;

;

@RunWith(MockitoJUnitRunner.class)
public class SavingsGoalServiceUnitTest {
    @InjectMocks
    private SavingsGoalService savingsGoalService;
    @Mock
    private Config config;
    @Mock
    private CurrencyAndAmountBuilder currencyAndAmountBuilder;
    @Mock
    private StarlingBankRequestService starlingBankRequestHandler;

    private UUID myUID = UUID.fromString("70d00b0f-19e8-46fb-9042-9b55f48e2180");

    @Test
    public void createSavingsGoal() {
        SavingsGoal savingsGoal = new SavingsGoal();
        savingsGoal.setCurrency("GBP");
        savingsGoal.setName("test");
        savingsGoal.setCurrencyAndAmount(new CurrencyAndAmount("GBP", new BigDecimal("12341234")));
        String payload = new Gson().toJson(savingsGoal);
        UUID myUID = UUID.fromString("70d00b0f-19e8-46fb-9042-9b55f48e2180");
        Mockito.doReturn(myUID).when(starlingBankRequestHandler).createSavingsGoal(payload, "nullnullnull/null");
        assert myUID == savingsGoalService.create(savingsGoal);
    }
}
