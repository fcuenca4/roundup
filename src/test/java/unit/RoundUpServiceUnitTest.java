package unit;

import com.demo.starling.model.Direction;
import com.demo.starling.model.Transaction;
import com.demo.starling.service.RoundUpService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RoundUpServiceUnitTest {
    @InjectMocks
    private RoundUpService roundUpService = new RoundUpService();

    @Test
    public void onExampleCalculateSavings() {
        List<Transaction> transactions = new ArrayList<>(4);
        transactions.add(new Transaction(new BigDecimal(4.35), Direction.OUTBOUND));
        transactions.add(new Transaction(new BigDecimal(5.20), Direction.OUTBOUND));
        transactions.add(new Transaction(new BigDecimal(0.87), Direction.OUTBOUND));
        transactions.add(new Transaction(new BigDecimal(22.87), Direction.INBOUND));
        BigDecimal savings = roundUpService.calculateSavings(transactions);
        assert savings.compareTo(new BigDecimal("1.58")) == 0;
    }
}
