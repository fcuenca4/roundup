# Starling Bank Interview

Develop a “round-up” feature for Starling customers using our public
developer API that is available to all customers and partners.
For a customer, take all the transactions in a given week and round them up to the
nearest pound. For example with spending of £4.35, £5.20 and £0.87, the round-up
would be £1.58. This amount should then be transferred into a savings goal , helping the
customer save for future adventures.

# Getting Started 
First of all, you must complete requirements inside config src/main//resources/application.yml
Those are:
* accessToken: your customer access token from starling web
*  accountUid: your account uuid
*  apiKeyUid: your apikeyuid from starling web
*  privateKeyPath: path to your generated key

Request Example:
curl -X PUT \
  http://localhost:8080/starling/roundup \
  -H 'Content-Type: application/json' \
  -d '{
	"name":"roundUpTest",
	"currency": "GBP",
	"currencyAndAmount":{
		"currency":"GBP",
		"minorUnits": 100
	}
}'

and then ... test the solution !

# Sequence Diarams
Here are some UML Sequence Diagrams to explain some of the different flows.

![alt text](https://tinypng.com/web/output/b4k5tr16jhwag1bft6k6nzk22jaeu86u/Get%20transaction%20ok.png)

![alt text](https://tinypng.com/web/output/pnwq17k86tc1ar4fp1q4k6pm2f9z07cq/Get%20transaction%20ok%20(3).png)


### Input Validations

Validations are made using *javax.validation* (model classes)
### Test 
Inside src/main/test are some unit tests related to the project.

### Class Responsibilities

* CredentialService is responsible to handle credential encryption/decryption ,
also calculates digest and loads  private key
* RoundUpService encapsulates business logic related to the problem. Method  
     * calculatesavings() calculates savings doing a round-up to the nearest currency.
     Inside Starling API, savings are mapped to *OUTBOUND* direction (from the customer account, to another). Also,i've made another round up (this time HALF_UP scaled to 2) because of how BigDecimal works and how Starling Bank is implemented ( showing 2 point scale in responses).
* SavingsGoalService is responsible to maintain all actions needed solve the problem. Those actions are:
    * createSavings
    * addMoney to specific savings
* TransactionService queries Starling Bank API to recover transactions of an specific customer account between two dates (from and to).

There are also 2 more services implemented:

* RequestHandler responsibility is to make HttpRequests and credentials generation, Digest and Authorization calculation
* StarlingBankRequestHandler execute starling's business api call to complete interview requirements. This class only manages to handle few logic. If problem is bigger, i must recommend to divide this class using Single Responsibility Principle i.e divide and implement classed based on specific logic.

### Improvements
There are certain questionable decisions to change or improve, for example:

* Private key path: Its not a good recomendation to leave the PRIVATE_KEY public or visible, even if its a private repository. The improve its to use any service that guarantees key recovery in execution time, for example saving keys in Amazon's S3 (Same logic is applied to secrets, we could use for example Travis-CI to store secrets as DB Users/Password/connection).
* Configuration File: All configurations are made inside application.yml file, the purpose of this file is not to configure the logic of the user, but of Spring (For example, dialect/ Serialization/Connections/etc).
* More IoC: There are certain scenarios where is recommended to use factory methods, builders or another creational design patterns to create our business objects.
* More Tests: There are no Funcional Tests inside this solution, it will be nice to add some.
    

